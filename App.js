class App{

  constructor(){
    this.myfunction = this.myfunction.bind(this);
    this.setEventos();
    //cambio
    this.checkEventos();
    this.verificarEstado = this.verificarEstado.bind(this);


  }

   setEventos(){

    const pestanas = document.querySelectorAll("#lista li");

    pestanas.forEach(element => {

       element.addEventListener("click",this.myfunction);

    });


    }

    checkEventos(){

        const checkboxs = document.querySelectorAll("#opciones input");
        
        checkboxs.forEach(element =>{

            element.addEventListener('change',this.verificarEstado,false);




        });
    }


     verificarEstado(event) {
        if(event.target.checked){
            const checkboxSeleccionado = event.currentTarget;
            const  completados = document.querySelector(".contenido #tab2"); 

            const span = document.querySelector("#"+checkboxSeleccionado.id+"s" )
            completados.appendChild(span);
            
            
            

        }else{
            const checkboxDesceleccionado = event.currentTarget;
            const  pendientes = document.querySelector(".contenido #tab1"); 
            const span  = document.querySelector("#"+checkboxDesceleccionado.id+"s" )
            pendientes.appendChild(span);
            


            
        }
    }








 myfunction(event){


    const pressed = event.currentTarget;
    const selected = document.querySelector(".selected");
    const selectedId = selected.dataset["id"];
    document.getElementById(selectedId).classList.add("hidden");
    selected.classList.remove("selected");
    pressed.classList.add("selected");
    const newSelectedId = pressed.dataset["id"];
    document.getElementById(newSelectedId).classList.remove("hidden");

}



}